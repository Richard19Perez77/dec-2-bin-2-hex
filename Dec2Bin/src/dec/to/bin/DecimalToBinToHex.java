package dec.to.bin;

import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import dec.to.bin.R.id;

public class DecimalToBinToHex {

	public EditText decimalXEditText, decimalYEditText, binaryXEditText,
			binaryYEditText, hexXEditText, hexYEditText;
	public Button convertButton, clearButton;
	public FragmentActivity activity;

	// get edit texts
	public void addViews(View v) {
		decimalXEditText = (EditText) v.findViewById(R.id.decimal_x_edittext);
		decimalYEditText = (EditText) v.findViewById(R.id.decimal_y_edittext);

		binaryXEditText = (EditText) v.findViewById(id.binary_x_edittext);
		binaryYEditText = (EditText) v.findViewById(R.id.binary_y_edittext);

		hexXEditText = (EditText) v.findViewById(R.id.hex_x_edittext);
		hexYEditText = (EditText) v.findViewById(R.id.hex_y_edittext);

		convertButton = (Button) v.findViewById(R.id.convert_button);
		convertButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				convertButton(v);
			}
		});

		clearButton = (Button) v.findViewById(R.id.clear_button);
		clearButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearButton(v);
			}
		});
	}

	protected void convertButton(View v) {
		Editable decX, decY, binX, binY, hexX, hexY;
		decX = decimalXEditText.getText();
		decY = decimalYEditText.getText();
		binX = binaryXEditText.getText();
		binY = binaryYEditText.getText();
		hexX = hexXEditText.getText();
		hexY = hexYEditText.getText();

		// check for one x
		boolean oneX = checkOneValues(decX, binX, hexX);
		if (!oneX) {
			Toast toast = Toast.makeText(activity.getBaseContext(),
					"Only one x needed", Toast.LENGTH_SHORT);
			toast.show();
		}

		boolean oneY = checkOneValues(decY, binY, hexY);
		if (!oneY) {
			Toast toast2 = Toast.makeText(activity.getBaseContext(),
					"Only one y needed", Toast.LENGTH_SHORT);
			toast2.show();
		}
	}

	private boolean checkOneValues(Editable dec, Editable bin, Editable hex) {
		if (dec.length() > 0) {
			if (bin.length() > 0)
				return false;
			else if (hex.length() > 0)
				return false;

			// if edit1 is the only one with text convert to other fields
			String decimalString = dec.toString();
			Integer temp = Integer.parseInt(decimalString);
			String hexString = Integer.toHexString(temp);

			binaryXEditText.setText("" + Integer.toBinaryString(temp));
			hexXEditText.setText(hexString);

			Toast toast2 = Toast.makeText(activity.getBaseContext(), hexString,
					Toast.LENGTH_SHORT);
			toast2.show();
		}

		if (bin.length() > 0)
			if (dec.length() > 0)
				return false;
			else if (hex.length() > 0)
				return false;

		if (hex.length() > 0)
			if (dec.length() > 0)
				return false;
			else if (bin.length() > 0)
				return false;

		return true;
	}


	protected void clearButton(View v) {
		// if any fields have text in them return to the hint state
		Editable s;
		s = decimalXEditText.getText();
		if (s.length() > 0)
			decimalXEditText.setText("");

		s = decimalYEditText.getText();
		if (s.length() > 0)
			decimalYEditText.setText("");

		s = binaryXEditText.getText();
		if (s.length() > 0)
			binaryXEditText.setText("");

		s = binaryYEditText.getText();
		if (s.length() > 0)
			binaryYEditText.setText("");

		s = hexXEditText.getText();
		if (s.length() > 0)
			hexXEditText.setText("");

		s = hexYEditText.getText();
		if (s.length() > 0)
			hexYEditText.setText("");
	}

	public void addActivity(FragmentActivity act) {
		activity = act;
	}
}